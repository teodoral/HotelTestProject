﻿using System;
using System.Web.Http;
using System.Web.Http.Results;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TeodoraLilic.Controllers;
using TeodoraLilic.Interfaces;
using TeodoraLilic.Models;

namespace UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        // TESTIRANJE 1.g)-----------------------------------------------------------------------
        [TestMethod]
        public void GetReturnsFestivalWithSameId()
        {
            // Arrange
            var mockRepository = new Mock<IHotelRepo>();
            mockRepository.Setup(x => x.GetById(42)).Returns(new Hotel { Id = 42 });

            var controller = new HoteliController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Get(42);
            var contentResult = actionResult as OkNegotiatedContentResult<Hotel>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(42, contentResult.Content.Id);
        }

        // TESTIRANJE 1.j)-----------------------------------------------------------------------
        [TestMethod]
        public void PutReturnsBadRequest()
        {
            // Arrange
            var mockRepository = new Mock<IHotelRepo>();
            var controller = new HoteliController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Put(8, new Hotel { Id = 21, Name = "Hotel 1", Year = 1990, Workers = 300, Rooms = 200, LanacId = 2 });

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
        }
    }
}
