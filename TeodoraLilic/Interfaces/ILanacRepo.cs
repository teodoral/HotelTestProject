﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeodoraLilic.Models;

namespace TeodoraLilic.Interfaces
{
    public interface ILanacRepo
    {
        IEnumerable<Lanac> GetAll();
        Lanac GetById(int id);
        IEnumerable<Lanac> GetOldest();
        IEnumerable<LanacDTO> GetWorkers();
        IEnumerable<Lanac> FilteredByRooms(int limit);
    }
}
