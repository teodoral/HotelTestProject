﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeodoraLilic.Models;

namespace TeodoraLilic.Interfaces
{
    public interface IHotelRepo
    {
        IEnumerable<Hotel> GetAll();
        Hotel GetById(int id);
        IEnumerable<Hotel> FilteredByWorkers(int zaposleni);
        void Add(Hotel hotel);
        void Edit(Hotel hotel);
        void Delete(Hotel hotel);
        IEnumerable<Hotel> FilteredByRooms(int min, int max);
    }
}
