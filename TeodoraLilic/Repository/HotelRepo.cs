﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using TeodoraLilic.Interfaces;
using TeodoraLilic.Models;
using System.Data.Entity;

namespace TeodoraLilic.Repository
{
    public class HotelRepo : IDisposable, IHotelRepo
    {
        ApplicationDbContext db = new ApplicationDbContext();

        public void Add(Hotel hotel)
        {
            db.Hoteli.Add(hotel);
            db.SaveChanges();
        }

        public void Delete(Hotel hotel)
        {
            db.Hoteli.Remove(hotel);
            db.SaveChanges();
        }

        public void Edit(Hotel hotel)
        {
            db.Entry(hotel).State = EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        public IEnumerable<Hotel> FilteredByRooms(int min, int max)
        {
            return db.Hoteli.Include(x => x.Lanac).Where(x => x.Rooms >= min && x.Rooms <= max).OrderByDescending(x => x.Rooms);
        }

        public IEnumerable<Hotel> FilteredByWorkers(int zaposleni)
        {
            return db.Hoteli.Include(x => x.Lanac).Where(x => x.Workers >= zaposleni).OrderBy(x => x.Workers);
        }

        public IEnumerable<Hotel> GetAll()
        {
            return db.Hoteli.Include(x => x.Lanac);
        }

        public Hotel GetById(int id)
        {
            return db.Hoteli.Include(x => x.Lanac).Where(x => x.Id == id).SingleOrDefault();
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}