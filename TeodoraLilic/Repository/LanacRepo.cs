﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TeodoraLilic.Interfaces;
using TeodoraLilic.Models;

namespace TeodoraLilic.Repository
{
    public class LanacRepo : IDisposable, ILanacRepo
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        
        public IEnumerable<Lanac> FilteredByRooms(int limit)
        {
            var groupedByRooms = db.Hoteli.GroupBy(
                x => x.LanacId,
                x => x.Rooms,
                (lanac, rooms) => new
                {
                    Id = lanac,
                    TotalRooms = rooms.Sum()
                }).OrderBy(x => x.TotalRooms);
            List<Lanac> FiltriraniLanci = new List<Lanac>();
            foreach(var x in groupedByRooms)
            {
                if(x.TotalRooms > limit)
                {
                    var Lanac = db.Lanci.Find(x.Id);
                    FiltriraniLanci.Add(Lanac);
                }
            }
            return FiltriraniLanci.AsEnumerable<Lanac>();
        }

        public IEnumerable<Lanac> GetAll()
        {
            return db.Lanci;
        }

        public Lanac GetById(int id)
        {
            return db.Lanci.Find(id);
        }

        public IEnumerable<Lanac> GetOldest()
        {
            return db.Lanci.OrderBy(x => x.Year).Take(2).OrderBy(x => x.Year);
        }

        public IEnumerable<LanacDTO> GetWorkers()
        {
            var groupedByWorkers = db.Hoteli.GroupBy(
                x => x.Lanac,
                x => x.Workers,
                (lanac, workers) => new LanacDTO
                {
                    Id = lanac.Id,
                    LanacName = lanac.Name,
                    AvarageWorkers = workers.Sum()/db.Hoteli.Where(x =>x.LanacId == lanac.Id).Count()
                }).OrderByDescending(x => x.AvarageWorkers);
            return groupedByWorkers;
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}