﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeodoraLilic.Models
{
    public class LanacDTO
    {
        public int Id { get; set; }
        public string LanacName { get; set; }
        public decimal AvarageWorkers { get; set; }
    }
}