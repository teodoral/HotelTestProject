﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TeodoraLilic.Models
{
    public class Lanac
    {
        public int Id { get; set; }
        [Required]
        [StringLength(75, ErrorMessage = "Naziv moze imati najvise 75 karaktera.")]
        public string Name { get; set; }
        [Required]
        [Range(1851, 2009, ErrorMessage = "Godina osnivanja mora biti u intervalu izmedju 1850 i 2010.")]
        public int Year { get; set; }
    }
}