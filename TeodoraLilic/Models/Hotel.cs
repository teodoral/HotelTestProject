﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TeodoraLilic.Models
{
    public class Hotel
    {
        public int Id { get; set; }
        [Required]
        [StringLength(80, ErrorMessage = "Naziv do 80 karaktera.")]
        public string Name { get; set; }
        [Required]
        [Range(1950, 2020, ErrorMessage = "Godina otvaranja mora biti u intervalu 1950-2020")]
        public int Year { get; set; }
        [Required]
        [Range(2, Int32.MaxValue, ErrorMessage = "Broj zaposlenih mora biti veci od 1.")]
        public int Workers { get; set; }
        [Range(10, 999, ErrorMessage = "Broj soba mora biti veci od 9 i manji od 1000")]
        public int Rooms { get; set; }
        public Lanac Lanac { get; set; }
        public int LanacId { get; set; }
    }
}