﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeodoraLilic.Models
{
    public class FilterHotel
    {
        public int Min { get; set; }
        public int Max { get; set; }
    }
}