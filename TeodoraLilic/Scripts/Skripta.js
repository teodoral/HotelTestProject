﻿$(document).ready(function () {
    var protocol = "http://";
    var host = window.location.host;
    var lanciEndpoint = "/api/lanci/";
    var hoteliEndpoint = "/api/hoteli/";
    var pretragaEndpoint = "/api/kapacitet";
    var tradicijaEndpoint = "/api/tradicija";
    var formAction = "Create";
    var token = sessionStorage.getItem("token");
    var headers = {};

    $(window).on("load", loadTable);

    // REGISTRACIJA I PRIJAVA/ODJAVA KORISNIKA

    //otvaranje forme za registraciju
    $("#btnRegistracija").on("click", function () {
        $("#dugmiciPocetak").css("display", "none");
        $("#registracija").css("display", "block");
    });

    //zatvaranje forme za registraciju
    $("#btnOdustani").on("click", function () {
        $("#dugmiciPocetak").css("display", "block");
        $("#registracija").css("display", "none");
    });

    //registracija
    $("#formaRegistracija").submit(function (e) {
        e.preventDefault();

        var userName = $("#emailReg").val();
        var password = $("#pwdReg").val();
        var passwordConfirm = $("#pwdConfirm").val();

        var sendData = {
            "Email": userName,
            "Password": password,
            "ConfirmPassword": passwordConfirm
        };
        console.log(sendData);

        $.ajax({
            type: "POST",
            url: protocol + host + "/api/Account/Register",
            data: sendData
        }).done(function (data) {
            $("#emailReg").val('');
            $("#pwdReg").val('');
            $("#pwdConfirm").val('');
            $("#porukaReg").css("display", "block");
            refreshTable();
        }).fail(function (data) {
            alert("Greska prilikom registracije");
            });
    });

    //otvaranje forme za prijavu
    $("#btnPrijavSe").on("click", function () {
        $("#dugmiciPocetak").css("display", "none");
        $("#registracija").css("display", "none");
        $("#porukaReg").css("display", "none");
        $("#prijava").css("display", "block");
    });

    //otvaranje forme za prijavu
    $("#btnPrijava").on("click", function () {
        $("#dugmiciPocetak").css("display", "none");
        $("#prijava").css("display", "block");
    });

    //zatvaranje forme za prijavu
    $("#btnOdustaniPrijava").on("click", function () {
        $("#dugmiciPocetak").css("display", "block");
        $("#prijava").css("display", "none");
    });

    //prijava
    $("#formaPrijava").submit(function (e) {
        e.preventDefault();

        var userName = $("#email").val();
        var password = $("#pwd").val();

        var sendData = {
            "grant_type": "password",
            "username": userName,
            "password": password
        };
        console.log(sendData);

        $.ajax({
            type: "POST",
            url: protocol + host + "/Token",
            data: sendData
        }).done(function (data) {
            token = data.access_token;
            $("#prijava").css("display", "none");
            $("#prijavljeniKorisnik").css("display", "block");
            $("#korisnik").empty().append(data.userName);
            $("#filteri").css("display", "block");
            $("#dodavanje").css("display", "block");
            refreshTable();
            loadChains();
        }).fail(function (data) {
            alert("Greska prilikom prijave");
        });
    });

    //odjava
    $("#btnOdjava").on("click", function () {
        token = null;
        headers = {};
        $("#pretraga").css("display", "none");
        $("#filteri").css("display", "none");
        $("#prijavljeniKorisnik").css("display", "none");
        $("#dodavanje").css("display", "none");
        $("#dugmiciPocetak").css("display", "block");
        refreshTable();
        $("#email").val('');
        $("#pwd").val('');
    });

    // RAD SA OBJEKTIMA

    // -------------------------- BRISANJE ------------------------------
    // priprema objekta za brisanje
    $("body").on("click", "#linkDelete", deleteHotel);

    // brisanje nekretnine
    function deleteHotel() {
        var deleteId = this.name;
        var deleteUrl = protocol + host + hoteliEndpoint + deleteId.toString();
        console.log(deleteUrl);

        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        $.ajax({
            url: deleteUrl,
            type: "DELETE",
            headers: headers

        })
            .done(function (data, status) {
                refreshTable();
            })
            .fail(function (data, statsus) {
                alert("Desila se greska pri pokusaju brisanja hotela");
            });
    }

    // --------------------- PRETRAGA HOTELA PO KAPACITETU----------------------------

    $("#formaPretraga").submit(function (e) {
        e.preventDefault();

        // korisnik mora biti ulogovan
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        var min = $("#min").val();
        var max = $("#max").val();

        // objekat koji se salje
        var sendData = {
            "Min": min,
            "Max": max
        };
        console.log(sendData);
        $.ajax({
            type: "POST",
            url: protocol + host + pretragaEndpoint,
            headers: headers,
            data: sendData
        }).done(function (data) {
            console.log(data);
            setTable(data);
        }).fail(function (data) {
            alert(data);
        });
    });

    // --------------------- DODAVANJE NOVOG HOTELA ----------------------------

    $("#formaDodavanje").submit(function (e) {
        e.preventDefault();

        // korisnik mora biti ulogovan
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        var lanac = $("#lanac").val();
        var naziv = $("#naziv").val();
        var godina = $("#godina").val();
        var sobe = $("#sobe").val();
        var zaposleni = $("#zaposleni").val();

        // objekat koji se salje
        var sendData = {
            "Name": naziv,
            "Year": godina,
            "Workers": zaposleni,
            "Rooms": sobe,
            "LanacId": lanac
        };
        console.log(sendData);
        $.ajax({
            type: "POST",
            url: protocol + host + hoteliEndpoint,
            headers: headers,
            data: sendData
        }).done(function (data, status) {
            console.log(data);
            $("#naziv").val('');
            $("#godina").val('');
            $("#sobe").val('');
            $("#zaposleni").val('');
            refreshTable();
        }).fail(function (data, status) {
            alert(data);
        });
    });

    //odustajanje od dodavanja
    $("#btnOdustaniDodavanje").on("click", function () {
        $("#naziv").val('');
        $("#godina").val('');
        $("#sobe").val('');
        $("#zaposleni").val('');
    });

    // UCITAVANJE PODATAKA I POPUNJAVANJE TABELA/DROPDOWN MENIJA
    // ucitavanje tabele s hotelima
    function loadTable() {
        var requestUrl = protocol + host + hoteliEndpoint;
        $.ajax({
            "type": "GET",
            "url": requestUrl
        }).done(function (data) {
            setTable(data);
        }).fail(function (data) {
            alert("Greska prilikom preuzimanja hotela!");
        });
    };

    // osvezavanje tabele s hotelima
    function refreshTable() {
        console.log("REFRESUJEM");
        $("#tabela").empty();
        loadTable();
    }

    // popunjavanje tabele s podacima o hotelima
    function setTable(data) {

        var $container = $("#tabela");
        $container.empty();

        console.log(data);

        // ispis tabele
        var table = $("<table id=mojaTabela></table>");
        if (token) {
            var header = $("<th>Naziv</th><th>Godina otvaranja</th><th>Broj soba</th><th>Broj zaposlenih</th><th>Lanac</th><th>Brisanje</th>");
        }
        else {
            header = $("<th>Naziv</th><th>Godina otvaranja</th><th>Broj soba</th><th>Broj zaposlenih</th><th>Lanac</th>");
        }

        table.append(header);
        for (i = 0; i < data.length; i++) {
            // prikazujemo novi red u tabeli
            var row = "<tr>";
            // prikaz podataka
            var displayData = "<td>" + data[i].Name + "</td><td>" + data[i].Year + "</td><td>" + data[i].Rooms + "</td><td>" + data[i].Workers + "</td><td>" + data[i].Lanac.Name + "</td>";
            var stringId = data[i].Id.toString();
            var displayDelete = "<td><p>[<a id=linkDelete name=" + stringId + ">Brisanje</a>]</p></td>";
            if (token) {
                row += displayData + displayDelete + "</tr>";
            }
            else {
                row += displayData + "</tr>";
            }
            table.append(row);
        }
        // ispis novog sadrzaja
        $container.append(table);
        $("#mojaTabela").addClass("myTable"); // OVDE RADI DODAVANJE KLASE, ALI NE SA BOOTSTRAP-om
    };

    // ucitavanje lanaca
    function loadChains() {
        var dropdownUrl = protocol + host + lanciEndpoint;
        var selectedChain = $("#lanac");
        selectedChain.empty();

        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }
        console.log("DROPDOWN URL: " + dropdownUrl);
        $.ajax({
            url: dropdownUrl,
            type: "GET",
            headers: headers
        }).done(function (data, status) {
            console.log(data);
            setChainsDropdown(data, status);
        }).fail(function (data, status) {
            alert("Desila se greska pri ucitavanju lanaca hotela");
        });

        //popunjavanje dropdown liste
        function setChainsDropdown(data, status) {
            var selectedChain = $("#lanac");
            selectedChain.empty();

            if (status === "success") {
                for (i = 0; i < data.length; i++) {
                    var option = "<option value=" + data[i].Id + ">" + data[i].Name + "</option>";
                    selectedChain.append(option);
                }
            }
            else {
                alert("Greska prilikom ucitavanja lanaca u padajuci meni!");
            }
        };

        //otvaranje liste s tradicijom
        $("#btnUcitavanje").on("click", function () {
            loadTradition();
            $("#btnUcitavanje").css("display", "none");
            $("#lista").css("display", "block");
        });

        //ucitavanje tradicije iz baze
        function loadTradition() {
            if (token) {
                headers.Authorization = 'Bearer ' + token;
            }
            var requestUrl = protocol + host + tradicijaEndpoint;
            console.log("URL tradicija: " + requestUrl);
            $.ajax({
                "type": "GET",
                "url": requestUrl
            }).done(function (data) {
                console.log(data);
                setTradition(data);
            }).fail(function (data) {
                alert("Greska prilikom ucitavanja tradicije!");
            });
        };

        // popunjavanje liste s tradicijom
        function setTradition(data) {
            var $container = $("#lista");
            $container.empty();

            console.log(data);

            // ispis tabele
            var lista = $("<ol></ol>");
            for (i = 0; i < data.length; i++) {
                var item = "<li>" + data[i].Name + " (osnovan " + data[i].Year + ". godine)</li>";
                lista.append(item);
            }
            $container.append(lista);
        };
    }
}) // KRAJ