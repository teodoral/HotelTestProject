﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TeodoraLilic.Interfaces;
using TeodoraLilic.Models;

namespace TeodoraLilic.Controllers
{
    public class HoteliController : ApiController
    {
        IHotelRepo _repository { get; set; }

        public HoteliController(IHotelRepo repository)
        {
            _repository = repository;
        }

        public IEnumerable<Hotel> Get()
        {
            return _repository.GetAll();
        }

        public IHttpActionResult Get(int id)
        {
            var hotel = _repository.GetById(id);
            if(hotel == null)
            {
                return NotFound();
            }
            return Ok(hotel);
        }

        public IEnumerable<Hotel> GetByWorkers(int zaposleni)
        {
            return _repository.FilteredByWorkers(zaposleni);
        }

        [Authorize]
        public IHttpActionResult Post(Hotel hotel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _repository.Add(hotel);
            return CreatedAtRoute("DefaultApi", new { id = hotel.Id }, hotel);
        }

        private bool HotelExists(int id)
        {
            return _repository.GetById(id) != null;
        }

        public IHttpActionResult Put(int id, Hotel hotel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != hotel.Id)
            {
                return BadRequest();
            }
            try
            {
                _repository.Edit(hotel);
            }
            catch
            {
                if (!HotelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Ok(hotel);
        }
        [Authorize]
        public IHttpActionResult Delete(int id)
        {
            var hotel = _repository.GetById(id);
            if (hotel == null)
            {
                return NotFound();
            }
            _repository.Delete(hotel);
            return Ok();
        }

        [Authorize]
        [Route("api/kapacitet")]
        public IEnumerable<Hotel> Post(FilterHotel filter)
        {
            int min = filter.Min;
            int max = filter.Max;
            return _repository.FilteredByRooms(min, max);
        }
    }
}
