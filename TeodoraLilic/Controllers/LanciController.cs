﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TeodoraLilic.Interfaces;
using TeodoraLilic.Models;

namespace TeodoraLilic.Controllers
{
    public class LanciController : ApiController
    {
        ILanacRepo _repository { get; set; }

        public LanciController(ILanacRepo repository)
        {
            _repository = repository;
        }

        public IEnumerable<Lanac> Get()
        {
            return _repository.GetAll();
        }

        public IHttpActionResult Get(int id)
        {
            var lanac = _repository.GetById(id);
            if(lanac == null)
            {
                return NotFound();
            }
            return Ok(lanac);
        }

        [Route("api/tradicija")]
        public IEnumerable<Lanac> GetTradition()
        {
            return _repository.GetOldest();
        }

        [Route("api/zaposleni")]
        public IEnumerable<LanacDTO> GetWorkers()
        {
            return _repository.GetWorkers();
        }

        [Route("api/sobe")]
        public IEnumerable<Lanac> Post(Limit granica)
        {
            var limit = granica.Granica;

            return _repository.FilteredByRooms(limit);
        }
    }
}
