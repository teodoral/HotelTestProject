namespace TeodoraLilic.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedModel : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Hotels", "Name", c => c.String(nullable: false, maxLength: 80));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Hotels", "Name", c => c.String());
        }
    }
}
