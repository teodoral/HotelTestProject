namespace TeodoraLilic.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using TeodoraLilic.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<TeodoraLilic.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(TeodoraLilic.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            context.Lanci.AddOrUpdate(x => x.Id,
                new Lanac { Id = 1, Name = "Hilton Worldwide", Year = 1919},
                new Lanac { Id = 2, Name = "Marriott International", Year = 1927 },
                new Lanac { Id = 3, Name = "Kempinski", Year = 1897 });

            context.Hoteli.AddOrUpdate(x => x.Id,
                new Hotel { Id = 1, Name = "Sheraton Novi Sad", Year = 2018, Workers = 70, Rooms = 150, LanacId = 2 },
                new Hotel { Id = 2, Name = "Hilton Belgrade", Year = 2017, Workers = 100, Rooms = 242, LanacId = 1 },
                new Hotel { Id = 3, Name = "Palais Hansen", Year = 2013, Workers = 80, Rooms = 152, LanacId = 3 },
                new Hotel { Id = 4, Name = "Budapest Marriott", Year = 1994, Workers = 130, Rooms = 364, LanacId = 2 },
                new Hotel { Id = 5, Name = "Hilton Berlin", Year = 1991, Workers = 200, Rooms = 601, LanacId = 1 });
        }
    }
}
